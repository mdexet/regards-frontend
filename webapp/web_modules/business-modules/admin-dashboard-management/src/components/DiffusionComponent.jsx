/**
 * Copyright 2017-2020 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of REGARDS.
 *
 * REGARDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * REGARDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with REGARDS. If not, see <http://www.gnu.org/licenses/>.
 **/
import get from 'lodash/get'
import { browserHistory } from 'react-router'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import { AdminShapes } from '@regardsoss/shape'
import { CommonDomain } from '@regardsoss/domain'
import RaisedButton from 'material-ui/RaisedButton'
import { themeContextType } from '@regardsoss/theme'
import { i18nContextType } from '@regardsoss/i18n'
import DisplayIconsComponent from './DisplayIconsComponent'
import { DISPLAY_ICON_TYPE_ENUM } from '../domain/displayIconTypes'
import { DIFFUSION_PRODUCTS_PROPERTIES } from '../domain/diffusionProperties'
import DisplayPropertiesComponent from './DisplayPropertiesComponent'
import { ICON_TYPE_ENUM } from '../domain/iconType'
import { STEP_SUB_TYPES_ENUM } from '../domain/stepSubTypes'

const {
  displayNumber,
} = CommonDomain.DisplayBigNumbers

/**
 * DiffusionComponent
 * @author Théo Lasserre
 */
class DiffusionComponent extends React.Component {
  static propTypes = {
    project: PropTypes.string.isRequired,
    sessionStep: AdminShapes.SessionStep,
  }

  static contextTypes = {
    ...themeContextType,
    ...i18nContextType,
  }

  onClick = () => {
    const { project } = this.props
    browserHistory.push(`/admin/${project}/data/acquisition/datasource/monitor`)
  }

  render() {
    const { sessionStep } = this.props
    const {
      intl: { formatMessage }, moduleTheme: {
        selectedSessionStyle: {
          cardStyle, cardTitleDivStyle, cardContentStyle, cardButtonStyle, cardTitleTextStyle,
          raisedListStyle, listItemDivStyle, cardTitleStyle, propertiesTitleStyle, propertiesDivStyle,
          cardSubTitleTextStyle,
        },
      },
    } = this.context
    const inputRelated = get(sessionStep, 'inputRelated', 0)
    const outputRelated = get(sessionStep, 'outputRelated', 0)
    const runnings = get(sessionStep, `state.${ICON_TYPE_ENUM.RUNNING}`, 0)
    return (
      sessionStep
        ? <Card style={cardStyle}>
          <div style={cardTitleDivStyle}>
            <CardTitle
              title={formatMessage({ id: 'dashboard.selectedsession.DISSEMINATION.diffusion.title' })}
              subtitle={formatMessage({ id: 'dashboard.selectedsession.DISSEMINATION.diffusion.subtitle' }, { nbIn: displayNumber(inputRelated, 3), nbOut: displayNumber(outputRelated, 3) })}
              titleStyle={cardTitleTextStyle}
              subtitleStyle={cardSubTitleTextStyle}
              style={cardTitleStyle}
            />
            {runnings !== 0
              ? <DisplayIconsComponent
                  entity={sessionStep}
                  displayIconType={DISPLAY_ICON_TYPE_ENUM.NO_COUNT}
              />
              : null}
          </div>
          <CardText style={cardContentStyle}>
            <div style={listItemDivStyle}>
              <div style={propertiesTitleStyle}>
                <div style={propertiesDivStyle}>
                  {formatMessage({ id: 'dashboard.selectedsession.DISSEMINATION.diffusion.properties.products.title' })}
                </div>
                <DisplayPropertiesComponent
                  properties={DIFFUSION_PRODUCTS_PROPERTIES}
                  sessionStep={sessionStep}
                  stepSubType={STEP_SUB_TYPES_ENUM.DISSEMINATION}
                />
              </div>
            </div>
            <div style={cardButtonStyle}>
              <RaisedButton
                onClick={this.onClick}
                label={formatMessage({ id: 'dashboard.selectedsession.DISSEMINATION.diffusion.button.see-detail' })}
                primary
                style={raisedListStyle}
              />
            </div>
          </CardText>
        </Card> : null
    )
  }
}
export default DiffusionComponent

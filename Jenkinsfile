#!/usr/bin/env groovy

/**
 * Copyright 2017-2021 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of REGARDS.
 *
 * REGARDS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * REGARDS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with REGARDS. If not, see <http://www.gnu.org/licenses/>.
 **/

/**
 * Declaratve Jenkinsfile. The language is Groovy.
 * Contains the definition of a Jenkins Pipeline, is checked into source control
 * and is expected to be the reference.
 * To fully support multibranch builds without issues, we are using docker-compose to setup cots for each build.
 *
 * @author Léo Mieulet
 * @see https://jenkins.io/doc/book/pipeline/jenkinsfile/
 */
pipeline {
    options {
        buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '2'))
        parallelsAlwaysFailFast()
    }
    agent { label 'unix-integration' }

    stages {
        stage('Preparation') {
            steps {
                echo "Jenkins node name = ${env.NODE_NAME}"
                echo "Current workspace = ${env.WORKSPACE}"
                sh 'cd jenkins/node && docker build -t rs_node . && chmod -R 0777 ${WORKSPACE}/webapp'
            }
        }
        stage('Install') {
            parallel {
                stage('-1-') {
                    stages {
                        stage('Install webapp') {
                            steps {
                                runFrontDockerImg("install", "")
                            }
                        }
                        stage('Install temporal') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/temporal")
                            }
                        }
                    }
                }
                stage('-2-') {
                    stages {
                        stage('Install data-with-picture-only') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/data-with-picture-only")
                            }
                        }
                        stage('Install last-version-only') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/last-version-only")
                            }
                        }
                        stage('Install enumerated') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/enumerated")
                            }
                        }
                        stage('Install numerical') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/numerical")
                            }
                        }
                        stage('Install numerical-range-criteria') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/numerical-range-criteria")
                            }
                        }
                        stage('Install string') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/string")
                            }
                        }
                        stage('Install fem-notify') {
                            steps {
                                runFrontDockerImg("install-plugin", "service/fem-notify")
                            }
                        }
                    }
                }
                stage('-3-') {
                    stages {
                        stage('Install full-text') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/full-text")
                            }
                        }
                        stage('Install example') {
                            steps {
                                runFrontDockerImg("install-plugin", "service/example")
                            }
                        }
                        stage('Install two-numerical') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/two-numerical")
                            }
                        }
                        stage('Install two-temporal') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/two-temporal")
                            }
                        }
                        stage('Install toponym') {
                            steps {
                                runFrontDockerImg("install-plugin", "criterion/toponym")
                            }
                        }
                        stage('Install fem-delete') {
                            steps {
                                runFrontDockerImg("install-plugin", "service/fem-delete")
                            }
                        }
                        stage('Install fem-edit') {
                            steps {
                                runFrontDockerImg("install-plugin", "service/fem-edit")
                            }
                        }
                    }
                }
            }
        }
        stage('Parallel build') {
            parallel {
                stage('-1-') {
                    stages {
                        stage('Build webapp') {
                            steps {
                                runFrontDockerImg("build_webapp", "")
                            }
                        }
                    }
                }
                stage('-2-') {
                    stages {
                        stage('Build data-with-picture-only') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/data-with-picture-only")
                            }
                        }
                        stage('Build last-version-only') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/last-version-only")
                            }
                        }
                        stage('Build enumerated') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/enumerated")
                            }
                        }
                        stage('Build fem-notify') {
                            steps {
                                runFrontDockerImg("build_plugin", "service/fem-notify")
                            }
                        }
                        stage('Build temporal') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/temporal")
                            }
                        }
                    }
                }
                stage('-3-') {
                    stages {
                        stage('Build full-text') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/full-text")
                            }
                        }
                        stage('Build numerical') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/numerical")
                            }
                        }
                        stage('Build numerical-range-criteria') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/numerical-range-criteria")
                            }
                        }
                        stage('Build string') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/string")
                            }
                        }
                        stage('Build fem-edit') {
                            steps {
                                runFrontDockerImg("build_plugin", "service/fem-edit")
                            }
                        }
                    }
                }
                stage('-4-') {
                    stages {
                        stage('Build two-numerical') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/two-numerical")
                            }
                        }
                        stage('Build two-temporal') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/two-temporal")
                            }
                        }
                        stage('Build toponym') {
                            steps {
                                runFrontDockerImg("build_plugin", "criterion/toponym")
                            }
                        }
                        stage('Build example') {
                            steps {
                                runFrontDockerImg("build_plugin", "service/example")
                            }
                        }
                        stage('Build fem-delete') {
                            steps {
                                runFrontDockerImg("build_plugin", "service/fem-delete")
                            }
                        }
                    }
                }
            }
        }

        stage('Final') {
            parallel {
                stage('Deploy Docker image') {
                    steps {
                        // Copy the bundle inside the folder where apache container will be bundled
                        sh 'cp -R ./webapp/dist/prod jenkins/nginx/dist'
                        // build image from nginx, tag with version/branch, then push
                        sh 'cd jenkins/nginx && ./buildTagAndPush.sh'
                    }
                }
                stage('Lint webapp') {
                    steps {
                        runFrontDockerImg("lint_webapp", "")
                    }
                }
            }
        }
    }
    post {
        failure {
            tuleapNotifyCommitStatus status: 'failure', repositoryId: '872', credentialId: 'tuleap-front-ci-token'
            mattermostSend color: 'danger', message: "Build Failed - ${env.JOB_NAME}#${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)", text: "Changes: \n"+getChangeString()
        }
        success {
            tuleapNotifyCommitStatus status: 'success', repositoryId: '872', credentialId: 'tuleap-front-ci-token'
        }
        cleanup {
            echo 'POST-CLEANUP-TASK -- Rewrire owner and access rights, to avoid future build having files issues'
            sh 'chown -R jenkins:jenkins .'
            sh 'chmod -R u+rwx .'
            sh 'chmod -R g+rwx .'
        }
    }
}


@NonCPS
def getChangeString() {
    def changeString = ""

    echo "Gathering SCM changes"
    def changeLogSets = currentBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            changeString += " - ${entry.msg} [@${entry.author}]\n"
        }
    }

    if (!changeString) {
        changeString = " - No new changes"
    }
    return changeString
}


// Run a docker image
// @params script script to execute
// @params pluginFolder the path to the plugin folder
@NonCPS
runFrontDockerImg(script, pluginFolder) {
    sh 'docker run --rm -i \
        -v ${WORKSPACE}/webapp:/app_to_build \
        rs_node ./' + script + '.sh ' + pluginFolder
}

